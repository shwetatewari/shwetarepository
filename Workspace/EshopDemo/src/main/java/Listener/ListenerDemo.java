package Listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerDemo implements ITestListener
{

	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailure(ITestResult Result) {
		System.out.println("Test case failed is :"+Result.getName());
		
	}

	public void onTestSkipped(ITestResult Result) {
		System.out.println("Test case skipped is :"+Result.getName());
		
	}

	public void onTestStart(ITestResult Result) {
		System.out.println("*******************************************");
		System.out.println("Test case started :"+Result.getName());
		System.out.println("*******************************************");
		
	}

	public void onTestSuccess(ITestResult Result) {
		System.out.println("*******************************************");
		System.out.println("Test case passed is :"+Result.getName());
		System.out.println("*******************************************");
		
	}
    
}
